# Lista Telefônica

### Descrição do Projeto

 **Responsável**: Greici Lauxen

Sistema de uma lista telefônica, onde os usuários cadastrados tenham acesso ao nome da empresa/pessoa e contato. Apenas usuário administrador pode fazer as alterações e inserções no sistema, os demais usuários terão acesso apenas as informações de contato.

### Modelagem

 **Responsável**: Ismael

O sistema deve permitir o cadastro de usuários.
O sistema deve permitir a edição de usuários.
O sistema deve permitir o cadastro de contatos.
O sistema deve exibir a lista de contatos.
Os contatos devem conter o nome, o tipo (empresa ou pessoa) e o número de telefone.
Somente o usuário administrador pode inserir e editar os contatos.

Tabelas do banco de dados:

usuario
-id
-nome
-email
-senha
-tipo

contato
-id
-nome
-tipo
-telefone


### Prototipação

 **Responsável**: Maiel Kamphorst

