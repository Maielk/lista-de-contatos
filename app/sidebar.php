<!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.php">
        <div class="sidebar-brand-icon rotate-n-15">
          <i class="fas fa-laugh-wink"></i>
        </div>
        <div class="sidebar-brand-text mx-3">Lista de Contatos</div>
      </a>

      <!-- Divider -->
      <hr class="sidebar-divider my-0">

      <!-- Nav Item - Dashboard -->
      <li class="nav-item active">
        <a class="nav-link" href="index.php">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Lista</span></a>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider">

     <?php
      if(($_SESSION['tipo']) == "Administrador"){
          echo " <li class='nav-item'>
          <a class='nav-link' href='lista-usuarios.php'>
            <i class='fas fa-fw fa-chart-area'></i>
            <span>Usuários</span></a>
        </li>";

    }
      ?>
      <!-- Nav Item - Charts   
        <li class="nav-item">
        <a class="nav-link" href="lista-usuarios.php">
          <i class="fas fa-fw fa-chart-area"></i>
          <span>Usuários</span></a>
      </li>
      
      
      -->

      <!-- Nav Item - Tables -->
      <li class="nav-item">
        <a class="nav-link" href="lista-contatos.php">
          <i class="fas fa-fw fa-table"></i>
          <span>Contatos</span></a>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider d-none d-md-block">

      <!-- Sidebar Toggler (Sidebar) -->
      <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>

    </ul>
    <!-- End of Sidebar -->